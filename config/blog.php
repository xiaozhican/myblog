<?php
return [
    'name' => '我的博客',
    'title' => '我的博客',
    'subtitle' => 'http://blog.test/',
    'description' =>'我的博客',
    'author' =>'馨空',
    'page_image' => 'home-bg.jpg',
    'posts_per_page' => 5,
    'uploads' => [
        'storage' => 'public',
        'webpath' => '/storage/uploads'
    ]
];
